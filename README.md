# auth-app

Perform sign-in, sign-up functionality

The project is working on Golang, MySQL database and AngularJS.

Use go install && auth-app -port [portnumber] to run the project
e.g. go install && auth-app  -port 12000

To access some variable, the project contains app.toml file inside Config folder